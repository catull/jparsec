# Project info

This repository holds the source code for the JPARSEC project, started back in 2006 and still in development.

The project home page is located [here][1].
You will find pieces of information with regards to purpose, history and development of JPARSEC. The javadoc and some example programs are also provided. The source code posted there is updated periodically from this repository.

The project has its [wiki][2] page showing with many examples what can be done with the source code. There is also a [PDF Paper on arxiv][3] describing the library.

JPARSEC is the common core of some programs with a GUI available for downloading from the project home page. An easy to install manager program is offered to simplify the installation of these programs and the downloading of dependencies or new versions. JPARSEC is also the core of the Android application [ClearSKY][4].

# Java source/target level

Since the first versions of JPARSEC and the programs based on it the Java source and target has been 1.6. The Android program is also targeted to 1.6 to support old devices.



[1]: http://conga.oan.es/~alonso/doku.php?id=jparsec "JPARSEC home page"

[2]: http://conga.oan.es/~alonso/doku.php?id=jparsec_wiki "JPARSEC wiki"

[3]: https://arxiv.org/abs/1806.03088 "arXiv:1806.03088"

[4]: https://play.google.com/store/apps/details?id=jparsec.androidlite "ClearSKY Free"
