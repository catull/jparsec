package jparsec.ephem;

import jparsec.ephem.Ephem;
import jparsec.ephem.EphemerisElement;
import jparsec.ephem.Functions;
import jparsec.ephem.Target.TARGET;
import jparsec.ephem.planets.EphemElement;
import jparsec.io.FileIO;
import jparsec.observer.ObserverElement;
import jparsec.time.AstroDate;
import jparsec.time.TimeElement;
import jparsec.time.TimeElement.SCALE;

/**
 * Implementation and test for the Mallama & Hilton revision for the computation 
 * of more realistic planetary magnitudes.
 * @author T. Alonso Albi - OAN (Spain)
 * @version 1.0
 */
public class RevisedPlanetaryMagnitudes2018Test {
	
	/**
	 * Main test program.
	 * @param args Not used.
	 */
	public static void main(String args[]) {
		try {
			// Tests from file Ap_Mag_Output_V3.txt, only those from Earth:
			// target body, year, month, day, expected revised apparent magnitude
			// They are for 00:00 UT of the corresponding date
			String testFromEarth[] = new String[] {
					"MERCURY,2006,5,19,-2.477", "MERCURY,2006,6,15,0.181", "MERCURY,2003,5,7,7.167",
					"VENUS,2006,10,28,-3.917", "VENUS,2005,12,14,-4.916", "VENUS,2004,6,8,-3.090",
					"MARS,2003,8,28,-2.862", "MARS,2004,7,19,1.788", 
					"JUPITER,2004,9,21,-1.667",	"JUPITER,2010,9,21,-2.934",
					"SATURN,2032,12,25,-0.552", "SATURN,2026,9,18,0.567,false", 
					"URANUS,1970,3,28,5.381", "URANUS,2008,3,8,6.025", 
					"NEPTUNE,1970,11,23,7.997", "NEPTUNE,1990,4,27,7.828", "NEPTUNE,2009,8,17,7.701"
			};

			// Test all of them
			int failed = 0; // Counter for failed tests
			double errorTol = 0.001; // Tolerance in the magnitude to consider the test as failed
			for (int i = 0; i < testFromEarth.length; i++) {
				// Read test data
				String target = FileIO.getField(1, testFromEarth[i], ",", false);
				int year = Integer.parseInt(FileIO.getField(2, testFromEarth[i], ",", false));
				int month = Integer.parseInt(FileIO.getField(3, testFromEarth[i], ",", false));
				int day = Integer.parseInt(FileIO.getField(4, testFromEarth[i], ",", false));
				double expectedMag = Double.parseDouble(FileIO.getField(5, testFromEarth[i], ",", false));
				boolean rings = true;
				if (FileIO.getNumberOfFields(testFromEarth[i], ",", false) == 6)
					rings = Boolean.parseBoolean(FileIO.getField(6, testFromEarth[i], ",", false));
				
				// Create ephemeris objects and compute revised magnitude and error. 
				// Note observer is irrelevant since ephemerides will be geocentric
				TimeElement time = new TimeElement(new AstroDate(year, month, day), SCALE.UNIVERSAL_TIME_UTC);
				ObserverElement obs = new ObserverElement();
				EphemerisElement eph = new EphemerisElement(TARGET.valueOf(target), 
						EphemerisElement.COORDINATES_TYPE.APPARENT, EphemerisElement.EQUINOX_OF_DATE, EphemerisElement.GEOCENTRIC, 
						EphemerisElement.REDUCTION_METHOD.IAU_2006, EphemerisElement.FRAME.ICRF, EphemerisElement.ALGORITHM.JPL_DE430);
				//eph.preferPrecisionInEphemerides = false; // True (default) means previous mag = revised mag, 
														   	// false will set previous mag with old methods
				
				EphemElement ephem = Ephem.getEphemeris(time, obs, eph, false);
				
				double magRevised = RevisedPlanetaryMagnitudes2018.getRevisedPlanetMagnitude(time, obs, eph, ephem, rings);
				double error = expectedMag - magRevised;
				
				// Report
				System.out.println("TEST #"+(i+1)+": "+testFromEarth[i]);
				System.out.println("Previous magnitude:   "+Functions.formatValue(ephem.magnitude, 3));
				System.out.println("Revised magnitude:    "+Functions.formatValue(magRevised, 3));
				System.out.println("Expected-revised mag: "+Functions.formatValue(error, 3));
				if (Math.abs(error) > errorTol) {
					System.err.println("*** ERROR ***");
					failed ++;
				}
				System.out.println();
			}
			System.out.println("FAILED TESTS: "+failed+"/"+testFromEarth.length);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
